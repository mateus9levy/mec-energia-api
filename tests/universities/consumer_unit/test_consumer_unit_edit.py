import pytest
from universities.models import ConsumerUnit, Contract

from tests.test_utils import dicts_test_utils
from tests.test_utils import create_objects_test_utils

@pytest.mark.django_db
def test_edit_consumer_unit_and_contract():
    university_dict = dicts_test_utils.university_dict_1
    university = create_objects_test_utils.create_test_university(university_dict)
    consumer_unit_dict = dicts_test_utils.consumer_unit_dict_1
    consumer_unit = create_objects_test_utils.create_test_consumer_unit(consumer_unit_dict, university)
    distributor_dict = dicts_test_utils.distributor_dict_1
    distributor = create_objects_test_utils.create_test_distributor(distributor_dict, university)
    contract_dict = dicts_test_utils.contract_dict_1
    contract = create_objects_test_utils.create_test_contract(contract_dict,distributor,consumer_unit)




    updated_consumer_unit, updated_contract = ConsumerUnit.edit_consumer_unit_and_contract(consumer_unit, contract)
    assert updated_consumer_unit.name == consumer_unit['name']
    assert updated_consumer_unit.code == consumer_unit['code']
    assert updated_consumer_unit.is_active == consumer_unit['is_active']
    assert updated_contract.distributor == contract['distributor']
    assert updated_contract.start_date == contract['start_date']
    assert updated_contract.tariff_flag == contract['tariff_flag']
    assert updated_contract.supply_voltage == contract['supply_voltage']
    assert updated_contract.peak_contracted_demand_in_kw == contract['peak_contracted_demand_in_kw']
    assert updated_contract.off_peak_contracted_demand_in_kw == contract['off_peak_contracted_demand_in_kw']
